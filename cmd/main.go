package main

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/tmidy/microservice-cache/config"
	"gitlab.com/tmidy/microservice-cache/internal/rest"
)

func main() {
	parent := context.Background()
	ctx := log.Logger.WithContext(parent)

	rest.NewApi(ctx, config.GetConfig())
}
