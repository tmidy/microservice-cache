package telemetry

import (
	"context"
	"log"

	"gitlab.com/tmidy/microservice-cache/config"
	"go.opentelemetry.io/otel"

	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

func InitTracer(ctx context.Context, cfg config.Config) {
	exp, err := otlptracehttp.New(ctx, otlptracehttp.WithEndpoint(cfg.JaegerEndpoint), otlptracehttp.WithInsecure(), otlptracehttp.WithHeaders(
		map[string]string{
			"X-Scope-OrgID": "demo",
		},
	))

	// traceExporter, err := stdouttrace.New(
	// 	stdouttrace.WithPrettyPrint())
	if err != nil {
		log.Fatal(err)
	}

	tp := trace.NewTracerProvider(
		trace.WithBatcher(exp),
		trace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(cfg.JaegerServiceName),
		)),
		trace.WithSampler(trace.AlwaysSample()),
	)
	otel.SetTracerProvider(tp)

	prop := newPropagator()
	otel.SetTextMapPropagator(prop)
}

func newPropagator() propagation.TextMapPropagator {
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)
}

// shutdown := func(ctx context.Context) error {
// 	var err error
// 	for _, fn := range shutdownFuncs {
// 		err = errors.Join(err, fn(ctx))
// 	}
// 	shutdownFuncs = nil
// 	return err
// }
// // handleErr calls shutdown for cleanup and makes sure that all errors are returned.
// handleErr := func(inErr error) {
// 	err = errors.Join(inErr, shutdown(ctx))
// }

// Set up meter provider.
// Set up resource.
// 	res, err := newResource(cfg.JaegerServiceName, "1")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	meterProvider, err := newMeterProvider(res)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	otel.SetMeterProvider(meterProvider)
// }

// func newMeterProvider(res *resource.Resource) (*metric.MeterProvider, error) {
// 	metricExporter, err := stdoutmetric.New()
// 	if err != nil {
// 		return nil, err
// 	}

// 	meterProvider := metric.NewMeterProvider(
// 		metric.WithResource(res),
// 		metric.WithReader(metric.NewPeriodicReader(metricExporter,
// 			// Default is 1m. Set to 3s for demonstrative purposes.
// 			metric.WithInterval(3*time.Second))),
// 	)
// 	return meterProvider, nil
// }

// func newResource(serviceName, serviceVersion string) (*resource.Resource, error) {
// 	return resource.Merge(resource.Default(),
// 		resource.NewWithAttributes(semconv.SchemaURL,
// 			semconv.ServiceNameKey.String(serviceName),
// 			semconv.ServiceVersionKey.String(serviceVersion),
// 		))
// }
