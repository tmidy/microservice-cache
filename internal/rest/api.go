package rest

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/tmidy/microservice-cache/config"
	"gitlab.com/tmidy/microservice-cache/internal/rest/controller"
	"gitlab.com/tmidy/microservice-cache/internal/rest/service"
	"gitlab.com/tmidy/microservice-cache/internal/telemetry"
	"gitlab.com/tmidy/microservice-cache/pkg/redis"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/metric"
	oteltrace "go.opentelemetry.io/otel/trace"
	"golang.org/x/sync/errgroup"
)

const timeoutCancel = 5 * time.Second

var tracer = otel.Tracer("micro-service-cache")
var rollCnt metric.Int64Counter

func NewApi(ctx context.Context, cfg config.Config) {
	cfg.InitLog()
	ctx, stop := signal.NotifyContext(ctx, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	defer stop()
	redisClient, err := redis.NewRedisClient(ctx, cfg.CacheCfg)
	if err != nil {
		log.Ctx(ctx).Error().Err(err).Caller().Msg("error init redis")
	}

	keyService := service.NewKeyService(redisClient)
	keyController := controller.NewKeyController(keyService)

	telemetry.InitTracer(ctx, cfg)

	r := gin.New()
	r.Use(otelgin.Middleware("micro-service-cache"), func(c *gin.Context) {
		id := uuid.NewString()
		_, span := tracer.Start(c.Request.Context(), c.Request.URL.Path, oteltrace.WithAttributes(attribute.String("id", id)))
		fmt.Println(span.IsRecording())
		defer span.End()

		span.SetStatus(codes.Ok, "ok")
		// span.AddEvent()

		roll := 1 + rand.Intn(6)

		// Add the custom attribute to the span and counter.
		rollValueAttr := attribute.Int("roll.value", roll)
		span.SetAttributes(rollValueAttr)
		// rollCnt.Add(ctx, 1, metric.WithAttributes(rollValueAttr))

		// resp := strconv.Itoa(roll) + "\n"
		// if _, err := io.WriteString(c.Writer, resp); err != nil {
		// 	log.Printf("Write failed: %v\n", err)
		// }
	})

	r.GET("_healthcheck", func(ctx *gin.Context) {
		ctx.AbortWithStatus(http.StatusOK)
	})

	// keys
	keyGroup := r.Group("keys")

	keyGroup.GET("/:key", keyController.GetKey)
	keyGroup.POST("", keyController.SetKey)
	keyGroup.DELETE("/:key", keyController.DeleteKey)

	g := new(errgroup.Group)

	rhandler := otelhttp.NewHandler(r, "/")

	srv := http.Server{
		Addr:    cfg.Host,
		Handler: rhandler,
		// should be variabilized
		ReadTimeout:       1 * time.Second,
		WriteTimeout:      1 * time.Second,
		IdleTimeout:       30 * time.Second,
		ReadHeaderTimeout: 2 * time.Second,
	}

	g.Go(func() error {
		return srv.ListenAndServe()
	})

	g.Go(func() error {
		return listenSig(ctx, stop, &srv, redisClient)
	})

	if err := g.Wait(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Ctx(ctx).Error().Err(err).Caller().Msg("error occured shuting down...")
	}
}

func listenSig(ctx context.Context, stop context.CancelFunc, srv *http.Server, redisClient *redis.RedisClient) error {
	<-ctx.Done()
	stop()

	redisClient.Close()
	log.Ctx(ctx).Info().Caller().Msg("shutting down gracefully")
	_, cancel := context.WithTimeout(ctx, timeoutCancel)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Ctx(ctx).Err(err).Caller().Msg("Server forced to shutdown: ")
		return err
	} else {
		log.Warn().Msg("server has shutdown ok")
	}

	log.Warn().Msg("server exiting...")
	time.Sleep(2 * time.Second)
	return nil
}
