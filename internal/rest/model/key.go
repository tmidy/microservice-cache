package model

type KeyPayload struct {
	Name       string `json:"name" binding:"required"`
	Value      string `json:"value"`
	Expiration string `json:"expiration,omitempty"`
}
