package service

import (
	"context"
	"time"

	"gitlab.com/tmidy/microservice-cache/internal/rest/model"
)

type cacheClient interface {
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error
	Get(ctx context.Context, key string) ([]byte, time.Duration, error)
	Delete(ctx context.Context, key string) error
}

type KeyService struct {
	cache cacheClient
}

func NewKeyService(c cacheClient) KeyService {
	return KeyService{
		cache: c,
	}
}

func (k KeyService) Get(ctx context.Context, key string) (model.KeyPayload, error) {
	b, ttl, err := k.cache.Get(ctx, key)
	if err != nil {
		return model.KeyPayload{}, err
	}
	return model.KeyPayload{
		Name:       key,
		Expiration: ttl.String(),
		Value:      string(b),
	}, nil
}

func (k KeyService) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	return k.cache.Set(ctx, key, value, expiration)
}

func (k KeyService) Delete(ctx context.Context, key string) error {
	return k.cache.Delete(ctx, key)
}
