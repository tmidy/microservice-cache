package controller

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/tmidy/microservice-cache/internal/rest/model"
	keyserrors "gitlab.com/tmidy/microservice-cache/pkg/errors"
)

const defaultExpiry = time.Minute

type keyService interface {
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error
	Get(ctx context.Context, key string) (model.KeyPayload, error)
	Delete(ctx context.Context, key string) error
}

type KeyController struct {
	keyService keyService
}

func NewKeyController(kService keyService) KeyController {
	return KeyController{
		keyService: kService,
	}
}

// getBooks responds with the list of all book as JSON.
func (k KeyController) GetKey(c *gin.Context) {
	ctx := c.Request.Context()

	param := c.Param("key")
	if param == "" {
		handleError(ctx, c, keyserrors.ErrBadRequest)
		return
	}

	p, err := k.keyService.Get(ctx, param)
	if err != nil {
		handleError(ctx, c, err)
		return
	}

	c.IndentedJSON(http.StatusOK, p)
}

func (k KeyController) SetKey(c *gin.Context) {
	ctx := c.Request.Context()

	var payload model.KeyPayload
	if err := c.BindJSON(&payload); err != nil {
		handleError(ctx, c, errors.Join(keyserrors.ErrBadRequest, err))
		return
	}

	d, err := getExpiration(payload.Expiration)
	if err != nil {
		handleError(ctx, c, err)
	}

	if err := k.keyService.Set(ctx, payload.Name, payload.Value, d); err != nil {
		handleError(ctx, c, err)
		return
	}

	c.IndentedJSON(http.StatusOK, payload)
}

func (k KeyController) DeleteKey(c *gin.Context) {
	ctx := c.Request.Context()

	param := c.Param("key")
	if param == "" {
		handleError(ctx, c, keyserrors.ErrBadRequest)
		return
	}
	err := k.keyService.Delete(ctx, param)
	if err != nil {
		handleError(ctx, c, err)
		return
	}

	c.Status(http.StatusOK)
}

func getExpiration(expiration string) (time.Duration, error) {
	if expiration == "" {
		return defaultExpiry, nil
	}

	d, err := time.ParseDuration(expiration)
	if err != nil {
		return -1, err
	}

	return d, nil
}

func handleError(ctx context.Context, c *gin.Context, err error) {
	switch {
	case errors.Is(err, keyserrors.ErrBadRequest):
		c.AbortWithStatusJSON(http.StatusBadRequest, err)
	case errors.Is(err, keyserrors.ErrKeyNotFound):
		c.AbortWithStatus(http.StatusNotFound)
	default:
		log.Error().Err(err).Msg("an internal error occurred")
		c.Status(http.StatusInternalServerError)
	}
}
