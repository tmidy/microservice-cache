package errors

import "fmt"

var ErrKeyNotFound = fmt.Errorf("key was not found")
var ErrBadRequest = fmt.Errorf("wrong request")
