package redis

import (
	"context"
	"time"

	"github.com/bsm/redislock"
	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	"github.com/rs/zerolog/log"
	"gitlab.com/tmidy/microservice-cache/config"
	"gitlab.com/tmidy/microservice-cache/pkg/errors"
	"go.opentelemetry.io/otel"
)

const ttlLockTime = 1 * time.Second

const lockKey = "redis:lock:"

var tracer = otel.Tracer("micro-service-cache")

type RedisClient struct {
	rdb    *redis.Client
	locker *redislock.Client
}

func NewRedisClient(ctx context.Context, cfg config.CacheCfg) (*RedisClient, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:           cfg.RedisHost,
		Password:       cfg.RedisPassword, // no password set
		DB:             cfg.RedisDB,       // use default DB
		PoolSize:       100,
		MaxActiveConns: 10,
	})

	// Enable tracing instrumentation.
	if err := redisotel.InstrumentTracing(rdb); err != nil {
		return &RedisClient{}, err
	}

	// Enable metrics instrumentation.
	if err := redisotel.InstrumentMetrics(rdb); err != nil {
		return &RedisClient{}, err
	}

	if err := rdb.Ping(ctx).Err(); err != nil {
		return &RedisClient{}, err
	}

	locker := redislock.New(rdb)

	return &RedisClient{
		locker: locker,
		rdb:    rdb,
	}, nil
}

func (r *RedisClient) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	ctx, span := tracer.Start(ctx, "set-key")

	defer span.End()
	lock, err := r.acquire(ctx, lockKey+key)
	if err != nil {
		log.Err(err).Caller().AnErr("lockerr", err).Msg("error obtanining")
		return err
	}
	defer func() {
		log.Info().Caller().AnErr("release", lock.Release(ctx)).Msg("release lock")
	}()
	return r.rdb.Set(ctx, key, value, expiration).Err()
}

func (r *RedisClient) Get(ctx context.Context, key string) ([]byte, time.Duration, error) {
	lock, err := r.acquire(ctx, lockKey+key)
	if err != nil {
		return nil, -1, err
	}
	defer func() {
		log.Info().Caller().AnErr("release", lock.Release(ctx)).Msg("release lock")
	}()
	val := r.rdb.Get(ctx, key)
	if val.Err() != nil {
		switch val.Err() {
		case redis.Nil:
			return nil, -1, errors.ErrKeyNotFound
		default:
			log.Info().Caller().Err(err).Msg("an error occured while getting key")
			return nil, -1, val.Err()
		}
	}

	expiry := r.rdb.TTL(ctx, key)
	dur, err := expiry.Result()
	if err != nil {
		return nil, -1, err
	}

	b, err := val.Bytes()
	if err != nil {
		return nil, +1, err
	}
	return b, dur, nil
}

func (r *RedisClient) Delete(ctx context.Context, key string) error {
	lock, err := r.acquire(ctx, lockKey+key)
	if err != nil {
		return err
	}
	defer func() {
		log.Info().Caller().AnErr("release", lock.Release(ctx)).Msg("release lock")
	}()

	return r.rdb.Del(ctx, key).Err()
}

func (r *RedisClient) acquire(ctx context.Context, key string) (*redislock.Lock, error) {
	lock, err := r.locker.Obtain(ctx, key, ttlLockTime, nil)
	if err != nil {
		return nil, err
	}
	return lock, nil
}

func (r *RedisClient) Close() error {
	return r.rdb.Close()
}
