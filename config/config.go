package config

import (
	"os"
	"strconv"

	"github.com/rs/zerolog"
)

type Config struct {
	LogLevel string
	CacheCfg
	Env               string
	Host              string
	JaegerEndpoint    string
	JaegerServiceName string
}

type CacheCfg struct {
	RedisDB       int
	RedisUser     string
	RedisPassword string
	RedisHost     string
	RedisPort     string
}

func GetConfig() Config {
	return Config{
		LogLevel: getEnv("log_level", "debug"),
		CacheCfg: CacheCfg{
			RedisDB:       getEnvInt("redis_name", 0),
			RedisUser:     getEnv("redis_user", ""),
			RedisPassword: getEnv("redis_password", ""),
			RedisHost:     getEnv("redis_host", "localhost:6379"),
			RedisPort:     getEnv("redis_port", "6379"),
		},
		Env:  getEnv("env", "dev"),
		Host: getEnv("host", "localhost:9000"),

		JaegerEndpoint:    getEnv("JAEGER_ENDPOINT", "localhost:4318"),
		JaegerServiceName: getEnv("JAEGER_SERVICE_NAME", "micro-service-cache"),
	}
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}

func getEnvInt(key string, fallback int) int {
	if val, ok := os.LookupEnv(key); ok {
		i, err := strconv.Atoi(val)
		switch {
		case err == nil:
			return i
		default:
			return fallback
		}
	}
	return fallback
}

func (c *Config) InitLog() {
	level, err := zerolog.ParseLevel(c.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	zerolog.DefaultContextLogger = &zerolog.Logger{}
}
